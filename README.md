# UCSDblog

A GitLabs Pages for a UCSD DevOps blog

## Getting started

Clone the repo and build the docker image, if using VSCode, look at note below code block:

```sh
git clone https://gitlab.com/ucsdlibrary/development/ucsdblog.git

cd ucsdblog

docker-compose up
```

The Jekyll site will now be running at: `http://localhost:4000/development/ucsdblog/` (note for Mac users `localhost`
may be something different.

## Before Actually Posting

Create a new branch for your post and use the `./scripts/new-post.sh` helper to generate an empty post for you in the
`_posts` directory using the correct file naming convention.

```sh
git checkout -b my-new-post
./scripts/new-post.sh all-about-docker
```

## Add your posts or pages

-[Adding pages](https://jekyllrb.com/docs/pages/) or [adding posts](https://jekyllrb.com/docs/posts/) taken directly from the `Jekyll` documentation :

  **Pages**:
  The simplest way of adding a page is to add an HTML file in the root directory with a suitable filename. You can also write a page in Markdown using a  `.md`  extension which converts to HTML on build. For a site with a homepage, an about page, and a contact page, here’s what the root directory and associated URLs might look like:

```
.
├── about.md # => http://example.com/about.html
├── index.html # => http://example.com/
└── contact.html # => http://example.com/contact.html
```
If you have a lot of pages, you can organize them into subfolders. The same subfolders that are used to group your pages in your project’s source will then exist in the `_site` folder when your site builds. However, when a page has a _different_ permalink set in the front matter, the subfolder at `_site` changes accordingly.

```
. ├── about.md # => http://example.com/about.html
├── documentation # folder containing pages
│ 	└── doc1.md # => http://example.com/documentation/doc1.html
├── design # folder containing pages
│ 	└── draft.md # => http://example.com/design/draft.html
```

  More information can be found on the [Jekyll Page documentation](https://jekyllrb.com/docs/pages/)

**Posts**:
The `_posts` folder is where your blog posts live. You typically write posts in [Markdown](https://daringfireball.net/projects/markdown/), HTML is also supported.

To create a post, add a file to your `_posts` directory with the following format:
```
YEAR-MONTH-DAY-title.MARKUP
```

Where  `YEAR`  is a four-digit number,  `MONTH`  and  `DAY`  are both two-digit numbers, and  `MARKUP`  is the file extension representing the format used in the file. For example, the following are examples of valid post filenames:

```
2011-12-31-new-years-eve-is-awesome.md
2012-09-12-how-to-write-a-blog.md
```

All blog post files must begin with  [front matter](https://jekyllrb.com/docs/front-matter/)  which is typically used to set a  [layout](https://jekyllrb.com/docs/layouts/)  or other meta data. For a simple example this can just be empty:

```
---
layout:  post
title:  "Welcome  to  Jekyll!"
---

# Welcome

**Hello world**, this is my first Jekyll blog post.

I hope you like it!
```

More advanced page instructions can be found [here](https://jekyllrb.com/docs/posts/#including-images-and-resources)

## Pushing up to the repo

Once you have a new post/page ready all we need to do is push it up:

```
git add .

git commit -m "your message here"

git push origin <branch-name>
OR
git push origin trunk (if we're able to push to the trunk)
```

And create the merge request if needed! Once it's approved and merged to trunk, the website should update automatically!
