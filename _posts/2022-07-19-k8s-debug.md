---
layout: post
title:  "Using kubectl debug"
categories: kubernetes
---

### summary

I recently ran into a situation where I was trying to understand why our Comet application couldn't connect to a newly
created external postgres database. As far as I could tell, the credentials we had _should_ have been right. But I
wanted to verify on the cluster, and within the namespace, to be sure I wasn't missing anything.

In a situation with a running `Pod`, this is fairly simple. One can simply use [`kubectl exec`][kubectl-exec] to get a
shell and run debug commands.

In this case, I had failing application `Pod`'s, and none of the installed dependency `Pod`'s such as Solr and Redis
would give me a useful shell environment for testing.

Thankfully `kubectl` [provides another option][ephemeral-pod] in the form of [`kubectl debug`][kubectl-debug]. This could be used as an
alternative to the use case mentioned above also.

I also needed a `Pod`, in this case, to attach to for debugging, since as noted I didn't have a useful running `Pod`.

For this, I used [`kubectl run`][kubectl-run] and the k8s `pause` container to get a `Pod` that would run in a state
that would keep readiness/liveness probes happy so that I could use it for debugging.

### kubectl debug commands

To put it all together, here are the commands:

```sh
❯ kubectl run ephemeral-pod --image=k8s.gcr.io/pause --wait=true --restart=Never --namespace=comet-staging
pod/ephemeral-debug-pod created

❯ kubectl debug -it ephemeral-pod --image=alpine:latest --target=ephemeral-pod --namespace comet-staging
Targeting container "ephemeral-pod". If you don't see processes from this container it may be because the container runtime doesn't support this feature.
Defaulting debug container name to debugger-mxv2d.
If you don't see a command prompt, try pressing enter.
/ # run debug commands, install packages, etc.
/ # exit

❯ kubectl delete pod --namespace=comet-staging ephemeral-pod # delete the pod when finished
```

This turned out to be incredibly helpful. In my case I installed the `postgresql-client` package, tried connecting to
the database using `psql` and realized what the issue was, given the `Secret`, `ConfigMag` and environment variables
that would have also been available to the application `Pod` that was failing.

### helper scripts

This is something I actually plan to use quite often, so I then took the time to turn this into a set of shell functions
I can easily invoke, using [fzf][fzf] to prompt me for providing a namespace.

Here are those helper functions which can be added to your shell `rc` file of choice.

```sh
# Kubernetes setup a debug session pod in a namespace
# we assume the k8s context is already set
k8s-debug-start() {
  local namespace
  podname='ephemeral-debug-pod'
  namespace=$(kubectl get ns | awk '{print $1}' | fzf)
  kubectl run "$podname" --image=k8s.gcr.io/pause --wait=true --restart=Never --namespace="$namespace"
  kubectl debug -it "$podname" --image=alpine:latest --target="$podname" --namespace "$namespace"
}

# Kubernetes delete a debug session pod from a namespace
# we assume the k8s context is already set
k8s-debug-stop() {
  local namespace
  podname='ephemeral-debug-pod'
  namespace=$(kubectl get ns | awk '{print $1}' | fzf)
  kubectl delete pod --namespace="$namespace" "$podname"
}
```

[ephemeral-pod]:https://kubernetes.io/docs/tasks/debug/debug-application/debug-running-pod/#ephemeral-container
[fzf]:https://github.com/junegunn/fzf
[kubectl-debug]:https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#debug
[kubectl-exec]:https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#exec
[kubectl-run]:https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#run
