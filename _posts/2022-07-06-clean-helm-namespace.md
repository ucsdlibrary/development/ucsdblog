---
layout: post
title:  "Clean a Kubernetes namespace"
categories: kubernetes
---

We often find that when our review cluster gets overwhelmed with too many active deployments, new deployments can start
to fail.

One example is that Solr pods might fail to come up with `OOMKilled`, for example.

A quick solution to try, short of going to the farther extent of cycling `Nodes`, is to simply delete all current helm
deployments in a give namespace.

Here is a short shell function to assist with that.

```sh
# Delete all deployments in a namespace
# takes a single argument
# example: helm-clean starlight-review
helm-clean() {
  helm list --namespace "$1" --short | xargs -L1 helm delete --namespace "$1"
}
```

And here's a nicer version, if you have [fzf][fzf] installed:

```sh
# Delete all deployments in a namespace
# Uses FZF to prompt for selecting a namespace
helm-clean() {
  local namespace
  namespace=$(kubectl get ns | awk '{print $1}' | fzf)
  helm list --namespace "$namespace" --short | xargs -L1 helm delete --namespace "$namespace"
}
```

Example (non-fzf):

```sh
❯ helm-clean tidewater-review
release "surfliner-tidewater-164420155" uninstalled
```

Example (fzf, assumes you select `tidewater-review`):
```sh
❯ helm-clean
release "surfliner-tidewater-164420155" uninstalled
```

[fzf]:https://github.com/junegunn/fzf
