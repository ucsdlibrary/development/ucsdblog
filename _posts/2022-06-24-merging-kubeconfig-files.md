---
layout: post
title:  "Merging kubeconfig files into one"
categories: kubernetes
---

In the Library we currently are running two Kubernetes clusters, one for review application deployments, and the other
for staging and production deployments.

The result of this, is that in order to access each cluster locally, you will need to download a `kubeconfig` file from
each cluster.

## Typical Usage

In the following examples, we're assuming we have 2 `kubeconfig` files, one for each cluster mentioned above.

- `~/.kube/configs/rancher-ucsd-prod`
- `~/.kube/configs/rancher-ucsd-nonprod`

The typical usage pattern in connecting to a cluster then, might be variations on the following:

k9s:

```sh
❯ k9s --kubeconfig ~/.kube/configs/rancher-ucsd-prod
❯ k9s --kubeconfig ~/.kube/configs/rancher-ucsd-nonprod
```

kubectl:

```sh
❯ kubectl --kubeconfig ~/.kube/configs/rancher-ucsd-prod --namespace=xyz ...
❯ kubectl --kubeconfig ~/.kube/configs/rancher-ucsd-nonprod --namespace=xyz ...
```

helm:

```sh
❯ helm upgrade --kubeconfig /home/mcritchlow/.kube/configs/rancher-ucsd-prod ....
❯ helm upgrade --kubeconfig /home/mcritchlow/.kube/configs/rancher-ucsd-nonprod ....
```

## Merge/Flatten kubeconfig files

There is another alternative which I recently discovered, which is merging the 2 files into one. Note that this could be
done with 1-m files, so there is technically no limit.

This uses a feature built into `kubectl`, which is `kubectl config view --flatten`.

NOTE: you could do the following in one step, sending the output of the command directly to the default `kubeconfig`
file location.

```sh
❯ KUBECONFIG=~/.kube/configs/rancher-ucsd-prod:~/.kube/configs/rancher-ucsd-review \nkubectl config view --flatten > /tmp/merged_config
❯ cp /tmp/merged_config ~/.kube/config
```

Now that you have a single `kubeconfig` in the default file location that `kubectl`, `helm` and `k9s` will look, you can
invoke them directly.

Example:

```sh
❯ k9s
```

The issue now, is that rather than selecting the `kubeconfig` file you want to use, you need to specify the
[context][context] to look at specific cluster information.

For `k9s` you can do that within the UI itself by typing:

- `:`
- `contexts`

Then hitting Enter. You can then select the context you want.

For `kubectl` and `helm` commands, however, you will probably want to set the `context` before invoking the command. So
you can run something like the following:

First, list the contexts you have available, this might look something like:

```sh
❯ kubectl config get-contexts
CURRENT   NAME                             CLUSTER                          AUTHINFO          NAMESPACE
*         libcluster-prod                  libcluster-prod                  libcluster-prod
          libcluster-prod-lib-etcd-prod1   libcluster-prod-lib-etcd-prod1   libcluster-prod
          libcluster-prod-lib-etcd-prod2   libcluster-prod-lib-etcd-prod2   libcluster-prod
          libcluster-prod-lib-etcd-prod3   libcluster-prod-lib-etcd-prod3   libcluster-prod
          nonprodv2                        nonprodv2                        nonprodv2

```

This output show 4 contexts, 3 for the `libcluster-prod` cluster (you can ignore the `etcd` contexts, and 1 for the
`nonprodv2` cluster. The context currently in use is marked with a `*`, if relevant, in this case `libcluster-prod`
context is in use.

We can then pick/switch to a context:

```sh
❯ kubectl config use-context nonprodv2
Switched to context "nonprodv2".
```

And then perform commands against that cluster and context:

kubectl:

```sh
❯ kubectl --namespace=xyz ...
❯ kubectl --namespace=xyz ...
```

helm:

```sh
❯ helm upgrade ....
```

### Bonus: Use FZF to switch contexts

If you're using the [fzf][fzf] utility, you could make yourself a simple `fzf` shell function added to your `.bashrc`,
`.zshrc`, etc. to simplify switching contexts, like the following:

```sh
ksc() {
  kubectl config use-context $(kubectl config get-contexts --no-headers | awk {'print $2'} | fzf)
}
```

### Summary
In the end, one still has to provide the cluster and context information when dealing with multiple clusters. However,
I've found that switching the kubernetes `context` feels more direct and ergonomic than flipping between multiple
`kubeconfig` files. Your mileage may vary!

[context]:https://kubernetes.io/docs/reference/kubectl/cheatsheet/#kubectl-context-and-configuration
[fzf]:https://github.com/junegunn/fzf

