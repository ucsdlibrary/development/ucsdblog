---
layout: post
title:  "Git bisect is amazing"
categories: git
---

We recently had an issue come up with Shoreline. Our operations folks reported that while the Shoreline landing page
came up just fine, performing any searches resulted in errors.

At that time, our team was on a Surfliner workcycle, but we were not working on Shoreline at all. Which meant a few
things:

- We did not have any idea how long this bug had existed in staging and production
- It was not obvious when this was introduced. We suspected during the offcycle, but we had no way of knowing for sure

The first step was to see if I could replicate the behavior we were seeing in staging and production locally.
Thankfully, I could. So we had a known bad git commit (`HEAD`, current `trunk`). But, we did not have a bad commit.

I tried doing some manual rolling back of dependencies, since I suspected this was the root issue, but this was an
imprecise way of doing things, because I really didn't know which was the problem.

The error, however, indicated that the bug, at least at the surface, was manifesting through `blacklight`.

So, I found the last time we had updated `blacklight`, and rolled it back. I think rebuilt the docker compose dev
environment, populated the shoreline seed data, and sure enough, things worked!

Here's the problem that `blacklight` commit was 4 months older than `trunk`/`HEAD`. There were _hundreds_ of commits
between the two. While I could certainly have taken some educated guesses as to problematic commits to try rolling back
to, `git` provides a tool specifically for dealing with this issue.

Enter [git-bisect][git-bisect].

The idea is fairly simple actually. I think, and this was true for me also, there is a perception that there is some
kind of `git` magic at play when using `bisect` and therefore it's a complicated tool to use. Or, folks just haven't
been exposed to it at all.

In short, it's incredibly simple to use. It prompts you to tell the tool which commit is "bad" and then a commit back in
history that is "good". The tool will then "bisect", or do a binary search, and ask you in each commit whether the
commit is "good" or "bad". It is _entirely_ up to you to determine what "good" or "bad" means. In some cases this might
be running the test suite. In the case of shoreline this was more complicated. We didn't have a test that was failing,
so I had to build the dev environment, seed sample data, and then do a search. This was quite time consuming, but it was
systematic and so I could have a high confidence level on "good" vs "bad".

Here's a short example of commands for how this works:

The following set of commands gets you started:

```sh
❯ cd surfliner # your git repo
❯ git bisect start # enter bisect
status: waiting for both good and bad commits
❯ git bisect bad HEAD # we knew HEAD was "bad"
❯ git bisect good b21357ab # an old commit that was "good"
```

Now `git bisect` is going to pick a commit for you to test. Your job is to decide if it's "good" or "bad". Here are a
couple "good" / "bad" examples:

```sh
Bisecting: 276 revisions left to test after this (roughly 8 steps)
[fb23a7ff3899ed5ec1b78f35db4dbfad36a75758] starlight: use POSTGRESQL_DATABASE not POSTGRES_DB
....
....
❯ git bisect good
Bisecting: 69 revisions left to test after this (roughly 6 steps)
[c59fa952339f141238121cb9735ceb638d18738a] starlight: remove solr_utils gem
❯ git bisect bad
Bisecting: 34 revisions left to test after this (roughly 5 steps)
[617467d06fe810154cb0a07d949017b4691cc501] comet: bump resource limits/requests
❯ git bisect good
Bisecting: 17 revisions left to test after this (roughly 4 steps)
[e11bd21eadd5ba5a390685f23060b0b9f98e0851] comet: Add specs for unpublishing
```

Finally, you will get down to the last commit.

```sh
❯ git bisect bad
Bisecting: 0 revisions left to test after this (roughly 0 steps)
[3038d2fd1ba14be5f2c2ed7d08e8a91ec810d750] chore(deps): update dependency geoblacklight to v3.7.0
```

This commit _should_ be "bad". If you can confirm that it is. You can tell `git bisect`:

```sh
❯ git bisect bad
3038d2fd1ba14be5f2c2ed7d08e8a91ec810d750 is the first bad commit
commit 3038d2fd1ba14be5f2c2ed7d08e8a91ec810d750
Author: UCSD LibraryGitLabService <librarygitlabservice@ad.ucsd.edu>
Date:   Fri Jun 24 15:10:04 2022 +0000

    chore(deps): update dependency geoblacklight to v3.7.0

 shoreline/Gemfile      |  2 +-
 shoreline/Gemfile.lock | 38 +++++++++++++++++++-------------------
 2 files changed, 20 insertions(+), 20 deletions(-)
 ```

 And there we have it. We bisected our way through hundreds of commits in 8 steps and found our culprit, a
 `geoblacklight` update. After a `git revert` of that commit, Shoreline was up and running in staging and production
 again!

 To end your `bisect` session, simply run:

 ```sh
 ❯ git bisect reset
Already on 'trunk'
Your branch is up to date with 'origin/trunk'.
```

There are several other `bisect` options to explore, but I think these are the basics worth knowing to get started.

[git-bisect]:https://git-scm.com/docs/git-bisect
