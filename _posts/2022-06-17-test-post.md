---
layout: post
title:  "Kristian's test post"
categories: kristian update
---

This is just a test post!

I want to see if I can 

a) Push to trunk
b) actually create a post like this!

Thanks!

EDIT: adding a `date:` field to the front matter overrides the date it will create for you. It seems best to leave it alone! 