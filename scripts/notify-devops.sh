#!/usr/bin/env sh
blog_url='https://ucsdlibrary.gitlab.io/development/ucsdblog'

# check for new files using diff-tree with filter `A` (added file) in _posts directory
new_file=$(git diff-tree --diff-filter=A --relative=_posts --no-commit-id --name-only -r "$CI_COMMIT_SHA")

if [ -z "$CI_SLACK_WEBHOOK_URL" ]; then
  echo "Missing CI_SLACK_WEBHOOK_URL environment variable."
else
  if [ "$new_file" ]; then
    # get title from post; awk for just the title string; tr for removing quotes; sed for removing leading spaces/tabs
    title=$(grep "title:" "_posts/$new_file" | awk '{$1=""; print $0}' | tr -d '"' | sed -e 's/^[ \t]*//')

    message="A new blog post titled '$title' was published to the devops blog! $blog_url"

    curl -X POST -H 'Content-type: application/json' --data '{"text":"'"$message"'"}' "$CI_SLACK_WEBHOOK_URL"
  else
    echo "This commit did not include a new post, not sending Slack notification"
  fi
fi
