#!/usr/bin/env sh

post_name="$1"
if [ -z "$post_name" ]; then
  echo "You must supply a post name to create"
  echo "Use no spaces, and hyphens between words"
  echo "Example: ./scripts/new-post.sh something-neat-about-docker"
  exit 1
fi

post_filename="_posts/$(date '+%F')-${post_name}.md"
touch "$post_filename"
echo "Created post: $post_filename"
